from sqlalchemy.dialects.mssql.base import TINYINT

__author__ = 'Luan Rafael'

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from AzureKanbanForms.Factory import db
import datetime

engine = db.get_engine()
Base = declarative_base()

Session = sessionmaker(autoflush=True, bind=engine)

class Agenda(Base):

    __tablename__ = 'agendas'

    id = Column(Integer, primary_key=True)
    medico = Column(String(256))
    consultas = Column(Text)
    clinica = Column(TINYINT)
    dth = Column(DateTime, default=datetime.datetime.now)

    def __init__(self, medico, consultas, clinica):
        self.medico = medico
        self.consultas = consultas
        self.clinica = clinica


    def save(self):
        session = Session()
        session.add(self)
        session.commit()


def listar_agendas():
    session = Session()
    return session.query(Agenda).all()


def get_last(clinica, medico):
    session = Session()
    return session.query(Agenda).filter(Agenda.clinica==clinica).filter(Agenda.medico==medico).order_by(desc(Agenda.id)).first()