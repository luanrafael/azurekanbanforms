#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'
from sqlalchemy import *
import configparser
from sqlalchemy.pool import NullPool
import os
engines = {}

def get_engine(database='Database'):
    global engines

    if database not in engines:
        engine = create_engine(build_connection(database), echo=True, poolclass=NullPool)
        engines[database] = engine

    return engines[database]


def build_connection(database):
    config = configparser.ConfigParser()
    source_path = os.path.dirname(os.path.abspath(__file__))
    config.read(source_path + '\\config.txt')
    con = config.get(database,'connection') # ['connection']
    user = config.get(database,'user') # ['user']
    password = config.get(database,'pass') #['pass']
    host = config.get(database,'host') #['host']
    port = config.get(database,'port') #['port']
    schemma = config.get(database,'schemma') #['schemma']
    url = con + user + ':' + password + '@' + host + ':' + port + '/' + schemma + '?charset=utf8'
    return url.replace(' ', "")
