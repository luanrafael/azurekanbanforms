#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'


import AzureKanbanForms.kanban as kanban
from AzureKanbanForms.Utils import utils
import json
import logging

import logging
import os

def sincronizar_agenda(cliente, medico, agenda_atual, agenda_antiga):

    if agenda_atual is None and agenda_antiga is None:
        logging.info("Nada para sincronizar!")
        return

    cfg = utils.get_config()

    clientes_cfg = cfg["CLIENTES"][cliente]

    kanbanConsumer = kanban.KanbanConsumer(clientes_cfg['ORGANIZATION_ID'], clientes_cfg['TOKEN'], cfg,
                                           clientes_cfg['PROCESS_ID'])

    kanbanConsumer.cancel_all_tasks()

    agendas = comparar(kanbanConsumer, json.loads(agenda_antiga), json.loads(agenda_atual))

    logging.info("atualizar: %d", len(agendas['agenda_atualizar']))
    logging.info("desmarcar: %d", len(agendas['agenda_desmarcar']))

    if len(agendas['agenda_atualizar']) == 0 and len(agendas['agenda_desmarcar']) == 0:
        logging.info("Nada para sincronizar!")
        return


    for consulta in agendas['agenda_atualizar']:

        if not is_valid(consulta, cfg['EXCLUIR']):
            continue

        if 'instance_id' not in consulta or consulta['instance_id'] is None:
            kanbanConsumer.start_process(medico, consulta)
        else:
            kanbanConsumer.update_instance(medico, consulta)

    for consulta in agendas['agenda_desmarcar']:
        consulta['data'] = None
        kanbanConsumer.update_instance(medico, consulta)

    logging.info("terminei de sincronizar a agenda: %s", medico)


def is_valid(data, exclusoes):
    for e in exclusoes:
        if e.lower() in data['nome'].lower():
            logging.info("ignorando consulta de nome: %s", data['nome'])
            return False

    return True


def populate(kanbanConsumer, agenda):
    agenda_kanban = kanbanConsumer.instancias_agendadas()

    logging.info("agenda kanban: %d ", len(agenda_kanban))

    result = []

    for a in agenda:

        for b in agenda_kanban:
            try:

                if int(a['id']) == int(b['paciente_id']) and int(a['id']) != 0:
                    a['instance_id'] = b['instance_id']
                    a['task_id'] = b['task_id']
                    a['data_consulta'] = b['data_consulta']
                    continue

                # Compara se o primeiro nome e o telefone são correspondentes
                first_name_a = a['nome'].split(' ')[0].lower()
                first_name_b = b['paciente_nome'].split(' ')[0].lower()
                if first_name_b == first_name_a and a['telefone'] == b['telefone']:
                    a['instance_id'] = b['instance_id']
                    a['task_id'] = b['task_id']
                    a['data_consulta'] = b['data_consulta']
                
            except ValueError:
                continue

        if 'instance_id' not in a:
            for b in agenda_kanban:
                if a['nome'] == b['paciente_nome']:
                    a['instance_id'] = b['instance_id']
                    a['task_id'] = b['task_id']
                    a['data_consulta'] = b['data_consulta']
                    continue

        result.append(a)

    return result


def comparar(kanbanConsumer, agenda_antiga, agenda_atual):
    if agenda_antiga is None:
        agenda_antiga = []


    agenda_desmarcar = [a for a in agenda_antiga if not _in(a, agenda_atual)]

    agenda_atualizar = [a for a in agenda_atual if not _in(a, agenda_antiga, True)]

    return {"agenda_atualizar": populate(kanbanConsumer, agenda_atualizar),
            "agenda_desmarcar": populate(kanbanConsumer, agenda_desmarcar)}


def _in(a, agenda, compare_date=False):
    
    if len(agenda) == 0:
        return False

    # Id e Nome
    for b in agenda:
        if a['id'] == b['id'] and a['nome'].lower() == b['nome'].lower():
            if compare_date is True and a['data'] == b['data']:
                return True

            if compare_date is False:
                return True

    # So ID
    for b in agenda:
        if a['id'] == b['id'] and a['id'] > 0:
            if compare_date is True and a['data'] == b['data']:
                return True

            if compare_date is False:
                return True

    # Compara nome completo
    for b in agenda:
        if a['nome'].lower() == b['nome'].lower():
            if compare_date is True and a['data'] == b['data']:
                return True

            if compare_date is False:
                return True

    # Compara se o primeiro nome e o telefone são correspondentes
    first_name_a = a['nome'].split(' ')[0].lower()
    for b in agenda:
        first_name_b = b['nome'].split(' ')[0].lower()
        if first_name_b == first_name_a and a['telefone'] == b['telefone']:
            if compare_date is True and a['data'] == b['data']:
                return True

            if compare_date is False:
                return True

    return False

def xt():
    old = json.loads(
        '''[{"telefone": "99316-1487", "nome": "ROBSON ROBERTO SILVEIRA DOS SANTOS", "tipo": "C", "id": "78352402", "data": "2016-07-27 00:00:00"}, {"telefone": "3314-5775", "nome": "ALICE ANGRA ONEIDA", "tipo": "R", "id": "78348927", "data": "2016-07-27 00:00:00"}, {"telefone": "98155-2071", "nome": "MARIA CAROLINA COUTINHO", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3341-9700", "nome": "ROSANGELA SILVA GON\u00c7ALVES", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3711-5543", "nome": "ANGELA MARIA TOREZANI MELLERE", "tipo": "C", "id": "78352054", "data": "2016-07-27 00:00:00"}, {"telefone": "9-8833-1909", "nome": "GEISA GENARO RODRIGUES", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3345-1189", "nome": "GIL ROBERTO DA SILVA", "tipo": "C", "id": "78350049", "data": "2016-07-27 00:00:00"}, {"telefone": "99886-0204", "nome": "ANA LUZIA ATAIDE", "tipo": "C", "id": "78351617", "data": "2016-07-27 00:00:00"}, {"telefone": "", "nome": "ISABEL 1", "tipo": "", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "99890-4281", "nome": "DINAMARIA MENDES CORREA", "tipo": "C", "id": "78351587", "data": "2016-07-27 00:00:00"}, {"telefone": "99855-5782", "nome": "THIAGO MONTEIRO", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "99989-7778", "nome": "LUIS FELIPE SUETE GUIMARAES", "tipo": "C", "id": "78344970", "data": "2016-07-27 00:00:00"}, {"telefone": "3316-1409", "nome": "JUAN MURILO DE SOUZA RUFIM", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3315-0415", "nome": "RENATA BISSOLI BORGES GUIMARAES", "tipo": "C", "id": "78325051", "data": "2016-07-28 00:00:00"}, {"telefone": "99954-9380", "nome": "DELMA RIBEIRO OASCH", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "99887-0158", "nome": "PEDRO HENRIQUE MATOS SOUZA", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "3215-5499", "nome": "CREUZA NASCIMENTO VARGAS", "tipo": "C", "id": "78342317", "data": "2016-07-28 00:00:00"}, {"telefone": "99918-1377", "nome": "DAIANA PINHEIRO AMORIM", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "9-9941-5199", "nome": "SANDRO MATOS", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "9-8141-5570", "nome": "LUCIENE REZENDE FARIA RIBEIRO", "tipo": "C", "id": "78351966", "data": "2016-07-28 00:00:00"}, {"telefone": "99745-1047", "nome": "ITAMAR TEIXEIRA MENDES", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "99955-2191", "nome": "ROSICLEIA CORREIA LOUREIRO", "tipo": "C", "id": "78353187", "data": "2016-07-28 00:00:00"}, {"telefone": "98812-4726", "nome": "LUCIMARY NAVES QUEIROZ", "tipo": "C", "id": "78349359", "data": "2016-07-28 00:00:00"}, {"telefone": "9-9742-6659", "nome": "MARCIA SOARES DE BARROS", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "3022-2732", "nome": "NARA MARLI ZANELLA PIERI", "tipo": "C", "id": "78338630", "data": "2016-07-28 00:00:00"}, {"telefone": "99905-5736", "nome": "YARALICE FABRI PEREIRA CASTANHI", "tipo": "C", "id": "78351964", "data": "2016-08-03 00:00:00"}, {"telefone": "3286-8437", "nome": "SOLANGE AMBROSIO SANTORIO", "tipo": "C", "id": "78352712", "data": "2016-08-03 00:00:00"}, {"telefone": "3315-0338", "nome": "ELIANA PRETTI ZAGO", "tipo": "C", "id": "78323440", "data": "2016-08-03 00:00:00"}, {"telefone": "99871-4638", "nome": "CLEITON FABIANO", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "99902-4992", "nome": "ARLIDIA GOMES ASTORI", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "99815-7784", "nome": "ADRIANA CLARINDA", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "9-9987-8854", "nome": "GISELE RABELO", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "9-8111-3133", "nome": "ROGERIO COSTA CORDEIRO", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "9-9963-8548", "nome": "LETICIA SANGLARD VALENTIM", "tipo": "C", "id": "78344963", "data": "2016-08-04 00:00:00"}, {"telefone": "99846-9658", "nome": "RORAN PESSIM", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "99846-9658", "nome": "EDUARDO RUGIM", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "3225-9235", "nome": "PRISCILA PINTO BARROSO", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "99720-5622", "nome": "WLIANY DE PAULA COSTA", "tipo": "C", "id": "0", "data": "2016-08-10 00:00:00"}, {"telefone": "9-9608-1767", "nome": "MARLEI CAMPOS OTONI NUNES", "tipo": "C", "id": "0", "data": "2016-08-10 00:00:00"}, {"telefone": "9-8829-6700", "nome": "RICARDO MIRANDA SEISEIRA", "tipo": "C", "id": "0", "data": "2016-08-11 00:00:00"}, {"telefone": "98112-8694", "nome": "MARCOS AURELIO FRANCO LAMBERTUCCI", "tipo": "C", "id": "0", "data": "2016-08-11 00:00:00"}, {"telefone": "9-9276-3195", "nome": "MARILIA SAMPAIO COUTINHO LIMA", "tipo": "C", "id": "78345779", "data": "2016-08-17 00:00:00"}, {"telefone": "99825-9024", "nome": "ELZINA MARIA PEREIRA CRUZ", "tipo": "C", "id": "78352120", "data": "2016-08-25 00:00:00"}, {"telefone": "99531-8934", "nome": "WANDERLY CAPUCHO ALVES", "tipo": "C", "id": "78352728", "data": "2016-08-31 00:00:00"}, {"telefone": "99941-7760", "nome": "VIRGINIA DE ALMEIDA CRATZ", "tipo": "C", "id": "78352790", "data": "2016-10-27 00:00:00"}, {"telefone": "1", "nome": "LIA PULLEN PARENTE", "tipo": "", "id": "0", "data": "2017-02-15 00:00:00"}, {"telefone": "81695769", "nome": "IGOR VIEIRA LEMOS", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "98514744", "nome": "TRASSY RODOLFO MACHADO", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "8875-0888", "nome": "GABRIEL KAYSER", "tipo": "C", "id": "0", "data": "2016-08-05 00:00:00"}, {"telefone": "3328-5009", "nome": "LARISSA ARRUD COELHO", "tipo": "C", "id": "0", "data": "2016-08-05 00:00:00"}, {"telefone": "3315-0415", "nome": "RENATA BISSOLI BORGES GUIMARAES", "tipo": "C", "id": "78325051", "data": "2016-08-28 00:00:00"}, {"telefone": "33145775", "nome": "WANIA MARIA ANGRA ONEIDA", "tipo": "R", "id": "78340659", "data": "2016-08-21 00:00:00"}, {"telefone": "9759-6933", "nome": "MARIA DE FATIMA LIMA DA SILVA", "tipo": "R", "id": "78337775", "data": "2016-08-12 00:00:00"}, {"telefone": "99985338", "nome": "ERINEIA DOS SANTOS MARCOLONGO DE JESUS", "tipo": "C", "id": "78331298", "data": "2016-08-04 00:00:00"}]''')
    atual = json.loads(
        '''[{"telefone": "3314-5775", "nome": "ALICE ANGRA ONEIDA", "tipo": "R", "id": "78348927", "data": "2016-07-27 00:00:00"}, {"telefone": "98155-2071", "nome": "MARIA CAROLINA COUTINHO", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3341-9700", "nome": "ROSANGELA SILVA GON\u00c7ALVES", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3711-5543", "nome": "ANGELA MARIA TOREZANI MELLERE", "tipo": "C", "id": "78352054", "data": "2016-07-27 00:00:00"}, {"telefone": "9-8833-1909", "nome": "GEISA GENARO RODRIGUES", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "3345-1189", "nome": "GIL ROBERTO DA SILVA", "tipo": "C", "id": "78350049", "data": "2016-07-27 00:00:00"}, {"telefone": "99886-0204", "nome": "ANA LUZIA ATAIDE", "tipo": "C", "id": "78351617", "data": "2016-07-27 00:00:00"}, {"telefone": "", "nome": "ISABEL 1", "tipo": "", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "99890-4281", "nome": "DINAMARIA MENDES CORREA", "tipo": "C", "id": "78351587", "data": "2016-07-27 00:00:00"}, {"telefone": "99855-5782", "nome": "THIAGO MONTEIRO", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "99989-7778", "nome": "LUIS FELIPE SUETE GUIMARAES", "tipo": "C", "id": "78344970", "data": "2016-07-27 00:00:00"}, {"telefone": "3316-1409", "nome": "JUAN MURILO DE SOUZA RUFIM", "tipo": "C", "id": "0", "data": "2016-07-27 00:00:00"}, {"telefone": "99954-9380", "nome": "DELMA RIBEIRO OASCH", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "99887-0158", "nome": "PEDRO HENRIQUE MATOS SOUZA", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "3215-5499", "nome": "CREUZA NASCIMENTO VARGAS", "tipo": "C", "id": "78342317", "data": "2016-07-28 00:00:00"}, {"telefone": "99918-1377", "nome": "DAIANA PINHEIRO AMORIM", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "9-9941-5199", "nome": "SANDRO MATOS", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "9-8141-5570", "nome": "LUCIENE REZENDE FARIA RIBEIRO", "tipo": "C", "id": "78351966", "data": "2016-07-28 00:00:00"}, {"telefone": "99745-1047", "nome": "ITAMAR TEIXEIRA MENDES", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "99955-2191", "nome": "ROSICLEIA CORREIA LOUREIRO", "tipo": "C", "id": "78353187", "data": "2016-07-28 00:00:00"}, {"telefone": "98812-4726", "nome": "LUCIMARY NAVES QUEIROZ", "tipo": "C", "id": "78349359", "data": "2016-07-28 00:00:00"}, {"telefone": "9-9742-6659", "nome": "MARCIA SOARES DE BARROS", "tipo": "C", "id": "0", "data": "2016-07-28 00:00:00"}, {"telefone": "3022-2732", "nome": "NARA MARLI ZANELLA PIERI", "tipo": "C", "id": "78338630", "data": "2016-07-28 00:00:00"}, {"telefone": "99905-5736", "nome": "YARALICE FABRI PEREIRA CASTANHI", "tipo": "C", "id": "78351964", "data": "2016-08-03 00:00:00"}, {"telefone": "3286-8437", "nome": "SOLANGE AMBROSIO SANTORIO", "tipo": "C", "id": "78352712", "data": "2016-08-03 00:00:00"}, {"telefone": "3315-0338", "nome": "ELIANA PRETTI ZAGO", "tipo": "C", "id": "78323440", "data": "2016-08-03 00:00:00"}, {"telefone": "99871-4638", "nome": "CLEITON FABIANO", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "99902-4992", "nome": "ARLIDIA GOMES ASTORI", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "99815-7784", "nome": "ADRIANA CLARINDA", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "9-9987-8854", "nome": "GISELE RABELO", "tipo": "C", "id": "0", "data": "2016-08-03 00:00:00"}, {"telefone": "9-8111-3133", "nome": "ROGERIO COSTA CORDEIRO", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "9-9963-8548", "nome": "LETICIA SANGLARD VALENTIM", "tipo": "C", "id": "78344963", "data": "2016-08-04 00:00:00"}, {"telefone": "99846-9658", "nome": "RORAN PESSIM", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "99846-9658", "nome": "EDUARDO RUGIM", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "3225-9235", "nome": "PRISCILA PINTO BARROSO", "tipo": "C", "id": "0", "data": "2016-08-04 00:00:00"}, {"telefone": "99720-5622", "nome": "WLIANY DE PAULA COSTA", "tipo": "C", "id": "0", "data": "2016-08-10 00:00:00"}, {"telefone": "9-9608-1767", "nome": "MARLEI CAMPOS OTONI NUNES", "tipo": "C", "id": "0", "data": "2016-08-10 00:00:00"}, {"telefone": "9-8829-6700", "nome": "RICARDO MIRANDA SEISEIRA", "tipo": "C", "id": "0", "data": "2016-08-11 00:00:00"}, {"telefone": "98112-8694", "nome": "MARCOS AURELIO FRANCO LAMBERTUCCI", "tipo": "C", "id": "0", "data": "2016-08-11 00:00:00"}, {"telefone": "9-9276-3195", "nome": "MARILIA SAMPAIO COUTINHO LIMA", "tipo": "C", "id": "78345779", "data": "2016-08-17 00:00:00"}, {"telefone": "99531-8934", "nome": "WANDERLY CAPUCHO ALVES", "tipo": "C", "id": "78352728", "data": "2016-08-31 00:00:00"}, {"telefone": "99941-7760", "nome": "VIRGINIA DE ALMEIDA CRATZ", "tipo": "C", "id": "78352790", "data": "2016-10-27 00:00:00"}, {"telefone": "1", "nome": "LIA PULLEN PARENTE", "tipo": "", "id": "0", "data": "2017-02-15 00:00:00"}]''')

    ret = comparar(old, atual)

    return ret

