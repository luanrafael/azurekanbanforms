#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Luan Rafael'

import re
from dateutil.relativedelta import relativedelta
import datetime
import io
import os
import json

def match_formula(string_search):
    pattern = r"([+-][\d]*[adhmM])"
    return do_match(pattern, string_search)


def formula_dict(string_formula):
    pattern = r"(?P<form_field>\[(.*?)\])(?P<trash>(.*?))(?P<operator>[+-])(?P<n>[\d]*)(?P<interval>[adhmMx])"
    return do_match(pattern, string_formula)

def is_shortcode(string_formula):
    pattern = r"(?P<form_field>\[(.*?)\])"
    return len(do_match(pattern, string_formula)) > 0

def match_expression(string_expression):
    pattern = r"(?P<method>\w+\(.*?\))(?P<extra>.*?[+-][\s\S]*?\d)?(?P<op>.*?(==|!=|<=|>=|>|<).*?)?"
    return do_match(pattern, string_expression)


def match_methods(string_expression):
    pattern = r"(?P<method>\w+\(.*?\))"
    return do_match(pattern, string_expression)


def match_args(string_expression):
    pattern = r"(?P<method>(\w+))(?P<args>\((.*?)\))"
    groups = do_match(pattern, string_expression)
    output = []

    for g in groups:
        g['args'] = None if g['args'] == "()" else g['args'].replace('(','').replace(')','')
        output.append(g)

    return output


def match_fields(string_expression):
    pattern = r"(?P<field>\[(.*?)\])"

    groups = do_match(pattern,string_expression)

    return groups



def match_formfield(string_search):
    pattern = r"\[(.*?)\]"
    return split_form_field(re.match(pattern, string_search, re.X).group())



def do_match(pattern, string_search):
    fre = re.compile(pattern)
    groups = fre.finditer(string_search)
    groups_dict = [f.groupdict() for f in groups]
    return groups_dict


def split_form_field(form_field):
    form_field = form_field.replace('[', '')
    form_field = form_field.replace(']', '')

    data_splitted = form_field.split('.')

    if len(data_splitted) == 0:
        return ''

    return data_splitted


def slugify(text):
    return re.sub(r'\W+', '-', text)


def dateadd(date, interval, str_qtd):
    qtd = int(str_qtd)

    if interval == 'h':
        dateadd_rel = relativedelta(hours=qtd)

    if interval == 'm':
        dateadd_rel = relativedelta(minutes=qtd)

    if interval == 'd':
        dateadd_rel = relativedelta(days=qtd)

    if interval == 'M':
        dateadd_rel = relativedelta(months=qtd)

    if interval == 'a':
        dateadd_rel = relativedelta(years=qtd)

    newdate = date + dateadd_rel

    return convert_date(newdate)


def convert_date(dt=0):

    if dt == 0:
        dt = datetime.date.today()

    return str(dt).split(' ')[0]


def trim_keys(dict):

    if len(dict) == 0 or type(dict) is list:
        return {}

    keys = list(dict.keys())
    dict_trim = {}
    for key in keys:
        dict_trim[re.sub(r' +',' ',key.rstrip())] = dict[key]

    return dict_trim


def validate_date(strdate):
    try:
        return datetime.datetime.strptime(strdate, '%d/%m/%Y')
    except:
        try:
            return datetime.datetime.strptime(strdate, '%Y-%m-%d')
        except:
            raise TypeError

    raise TypeError


def get_week(dt=0):

    if dt == 0:
        return datetime.date.today().isocalendar()[1]
    try:
        return validate_date(dt).isocalendar()[1]
    except TypeError:
        pass


def call_method(method, args):
    if method == 'semana':
        return get_week(args) if args is not None else get_week()


def get_config():
    source_path = os.path.dirname(os.path.abspath(__file__))
    with io.open(source_path + '\\config.json', mode='r',encoding="utf-8") as f:
        cfg = json.loads(f.read())
    f.close()
    return cfg




# BROKER IMPLEMENTATION
import requests


def send(fingerprint, cmd):
    broker_url = 'http://broker.kanban360.com.br:8090/cloud'
    data_post = {}
    data_post['idDest'] = 'CLI-' + fingerprint
    data_post['idConsumidor']='Avulso'
    data_post['idRmte']='WEB-' + fingerprint
    data_post['msgEnv']= cmd
    data_post['msgResp']= 50000
    data_post['msgRet']= ''
    
    try:
        requests.post(url=broker_url, data=data_post)
        return True
    except:
        return False