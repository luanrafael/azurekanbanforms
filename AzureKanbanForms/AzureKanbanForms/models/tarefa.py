from sqlalchemy.dialects.mssql.base import TEXT

__author__ = 'Luan Rafael'

from sqlalchemy import *
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.ext.declarative import declarative_base
import datetime
from AzureKanbanForms.models import helper
Base = declarative_base()


class Tarefa(Base):
     __tablename__ = 'tarefas'
     id = Column(Integer, primary_key=True)
     id_acao = Column(Integer)
     acao = Column(String(256))
     fingerprint = Column(String(256))

     def __init__(self, id_acao,  acao, fingerprint):
         self.id_acao = id_acao
         self.acao = acao
         self.fingerprint = fingerprint



