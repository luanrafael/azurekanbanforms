from sqlalchemy.dialects.mssql.base import TEXT

__author__ = 'Luan Rafael'

from sqlalchemy import *
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.ext.declarative import declarative_base
import datetime
from AzureKanbanForms.models import helper
Base = declarative_base()


class Service(Base):

    __tablename__ = 'services'

    id = Column(Integer, primary_key=True)
    data = Column(TEXT)
    created = Column(DateTime, default=datetime.datetime.now)
    modified = Column(DateTime, default=datetime.datetime.now, onupdate=datetime.datetime.now)

    def __init__(self, id, data):
        self.id = id
        self.data = data


def get(id):
    dbSession = helper.DBSession("kanbanforms")
    session = dbSession.get_session()
    try:
        result = session.query(Service).filter(Service.id == id).one()
        session.close()
        dbSession.close()
        return result
    except NoResultFound:
        dbSession.close()
        return None
