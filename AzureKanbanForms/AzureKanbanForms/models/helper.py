
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from AzureKanbanForms.Factory import db

Base = declarative_base()


class DBSession():
    def __init__(self, database='Database'):
        self.engine = db.get_engine(database)
        self.Session = sessionmaker(autoflush=True, bind=self.engine, expire_on_commit=False)

    def get_session(self):
        return self.Session()

    def close(self):
        self.engine.dispose()

    def save(self, model, dispose = False):

        if model.id is None:
            modelFind = None
        else:
            modelFind = self.get(model.__class__, model.id)

        session = self.Session()

        if modelFind is None:
            session.add(model)
        else:
            modelFind.data = model.data

        session.commit()

        try:
            session.expunge(model)
        except Exception as e:
            print("nok")

        session.close()

        if dispose is True:
            self.dispose()


    def get(self, classModel, id):
        session = self.Session()
        result = session.query(classModel).get(id)
        session.close()
        return result

    def dispose(self):
        self.engine.dispose()