__author__ = 'Luan Rafael'

from sqlalchemy.dialects.mssql.base import TINYINT

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
import datetime
from AzureKanbanForms.models import helper

Base = declarative_base()

class Xaklog(Base):
    
    __tablename__ = 'xaklogs'

    id = Column(Integer, primary_key=True)
    token = Column(String(56))
    dt_criacao = Column(DateTime, default=datetime.datetime.now)
    dt_alteracao = Column(DateTime, onupdate=datetime.datetime.now)
    dt_log = Column(String(6))
    conteudo = Column(Text)
    tipo = Column(TINYINT)

    def __init__(self, token, tipo, dt_log, conteudo):
        self.token = token
        self.tipo = tipo
        self.dt_log = dt_log
        self.conteudo = conteudo

def get(token, tipo, dt_log, session = helper.DBSession('xakomigo_logs').get_session(), close = True):
    log = session.query(Xaklog).filter(Xaklog.token==token).filter(Xaklog.tipo==tipo).filter(Xaklog.dt_log==dt_log).first()
    if close:
        session.close()
    return log

