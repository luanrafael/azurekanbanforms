#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'

import requests
import json
from Utils import utils
import logging
from AzureKanbanForms.Factory import db
from sqlalchemy import text


class KanbanConsumer():


    def __init__(self, organization_id, token, config, process_id):

        self.organization_id = organization_id
        self.token = token
        self.config = config
        self.process_id = process_id

    def get_forms(self, instance, task):

        instance_data = self.send_get('tasks', str(instance))
        forms = {}

        if instance_data is None or len(instance_data['data']['instances']) == 0:
            forms['instance'] = None
        else:
            forms['instance'] = utils.trim_keys(instance_data['data']['instances'][0]['form'])

        task_params = str(instance) + '/' + str(task)
        task_data = self.send_get('tasks', task_params)

        if task_data is None or 'task' not in task_data['data'] or len(task_data['data']['task']) == 0:
            forms['task'] = None
        else:
            forms['task'] = utils.trim_keys(task_data['data']['task'][0]['form'])

        return forms

    def send_get(self, endpoint, params, t=1):
        url = self._build_url(endpoint, params)

        try:
            data_request = requests.get(url)
            response = data_request.json()
            if ('error' in response and response['error'] == True) or data_request.status_code != 200:
                logging.info(response['message'])
                return None
            return response
        except:
            if t <= 3:
                return self.send_get(endpoint, params, t + 1)
            return None

        return None

    def start_process(self, medico, dados):

        logging.info("criando instancia")

        form = self._monta_form(medico, dados)

        data = {
            "token": self.token,
            "process_id": self.process_id,  # 2877
            "form": form
        }

        data_request = self.send_post('start_process', data)
        logging.info(data_request)

    def update_instance(self, medico, dados):

        if 'instance_id' not in dados:
            logging.info("Nao consegui atualizar, pois nao achei instancia")
            return False

        logging.info("atualizando instancia: %s", str(dados['instance_id']))

        instance = dados['instance_id']


        try:
            id = int(dados['id'])

            if id == 0:
                id = None

        except ValueError:
            id = None

        form = {
            'ID': id,
            'Nomepaciente': dados['nome'],
            'Medico': self.config[medico],
            'Dataconsulta': utils.convert_date(dados['data']) if dados['data'] is not None else "",
            'Tipodeconsulta': self.config['TIPOS'][dados['tipo']] if dados['tipo'] in self.config['TIPOS'] else 3,
            'Telefone': dados['telefone']
        }

        params = '0/' + str(instance)

        ret = self.send_put('processes', params, {'form': form})

        return ret is not None


    def cancel_task(self, instance, task_id):

        logging.info("cancelando tarefa %d da instancia %d", task_id, instance)

        data = {
            "status": 5,
            "form": {
                "Controledeconsulta": 2
            }
        }

        params = str(instance) + "/" + str(task_id)
        ret = self.send_put('tasks', params, data)

        logging.info(ret)

        return ret is not None

    def update_form(self, instance, form, newform):

        keys = list(form.keys())

        for k in keys:
            if k in newform and newform[k] != '##NAO_ACHEI_NADA##':
                form[k] = newform[k]

        params = '0/' + str(instance)
        put_result = self.send_put('processes', params, {'form': form})
        return put_result

    def update_column(self, instance_id, data):

        data_post = {}

        data_instance = self.get_tasks(instance_id)

        if data_instance is None:
            return None

        for task in data_instance['data']['instances'][0]['tasks']:
            if task['name'] == data['task'] and task['status'] < 3:
                data_post['task_id'] = task['task_id']
                break

        if 'task_id' not in data_post:
            return None

        columns = self.get_work_columns()

        if columns is None:
            return None

        for column in columns['data']:
            if column['name'] == data['column']:
                data_post['work_column'] = column['id']
                break

        users = self.get_users()

        if users is not None and 'owner' in data and data['owner'] is not None:
            for user in users['data']:
                if user['name'] == data['owner']:
                    data_post['owner'] = user['id']
                    break

        data_post['token'] = self.token

        ret = self.send_post("plan_task", data_post)

        return ret


    def get_users(self):
        users = self.send_get("users","#")
        return users

    def get_work_columns(self):
        columns = self.send_get("work_columns", str(self.process_id))
        return columns

    def get_tasks(self, instance_id):
        tasks = self.send_get("tasks", str(instance_id))
        return tasks

    def send_post(self, endpoint, data):

        url = self._build_url(endpoint)

        logging.info(data)

        data_request = requests.post(url, data=json.dumps(data), headers=self.config['HEADERS'])

        response = data_request.json()

        return response

    def send_put(self, endpoint, params, data):
        url = self._build_url(endpoint, params)

        logging.info(data)

        try:
            data_request = requests.put(url, data=json.dumps(data), headers=self.config['HEADERS'])
            response = data_request.json()
            if ('error' in response and response['error'] == True) or data_request.status_code != 200:
                return None
            return response
        except:
            return None

        return None

    def _build_url(self, endpoint, params=''):

        if params != '':
            url = self.config['URL_BASE'] + endpoint + '/' + self.token + '/' + params
        else:
            url = self.config['URL_BASE'] + endpoint

        logging.info(url)

        return url

    def instancias_agendadas(self):
        sql = text(
            'select * from superclient_vw_instancias_agenda where organization_id = %s;' % str(self.organization_id))
        engine = db.get_engine("Database_Kanban360")
        result = engine.execute(sql)

        ret = [self._to_dict(row) for row in result if self._to_dict(row) is not None]

        engine.dispose()
        return ret

    def _to_dict(self, row):

        if len(row) == 0 or row is None or row[1] is None:
            return None

        output = {}

        dados_paciente = row[1].replace("**", "").split(',')

        if len(dados_paciente) < 3:
            return None

        output['instance_id'] = row[0]
        output['paciente_id'] = dados_paciente[0]
        output['paciente_nome'] = dados_paciente[1]
        output['telefone'] = dados_paciente[2]
        output['data_consulta'] = dados_paciente[4]
        output['task_id'] = row[2]

        return output

    def _monta_form(self, medico, dados):

        try:
            id = int(dados['id'])

            if id == 0:
                id = None

        except ValueError:
            id = None

        form = {
            'Status': 16,
            'ID': id,
            'Nomepaciente': dados['nome'],
            'Emailpaciente': '',
            'Medico': self.config[medico],
            'Tipodeconsulta': self.config['TIPOS'][dados['tipo']] if dados['tipo'] in self.config['TIPOS'] else 3,
            'Indicacao': None,
            'Consentimento': None,
            'Categorizacao': None,
            'Tipodeacomodaca': None,
            'Datacirurgiaprocedimento': None,
            'ProxFUP': None,
            'DatadaIndicacao': None,
            'DataproxFUP': None,
            'Dataultimaconsulta': None,
            'Dataconsulta': utils.convert_date(dados['data']) if dados['data'] is not None else "",
            'Telefone': dados['telefone']
        }

        return form

    def cancel_all_tasks(self):
       
        sql = text('select instance_id,task_id from superclient_vw_instancias_agenda where task_id is not null and organization_id = :org_id;')
        engine = db.get_engine("Database_Kanban360")
        result = engine.execute(sql,org_id=self.organization_id)

        for row in result:
            if not self.cancel_task(row['instance_id'], row['task_id']):
                logging.info("nao consegui cancelar a tarefa")

        engine.dispose()

