#!/usr/bin/env python
# -*- coding: utf-8 -*-


from routes import app
import logging
import logging.config


import os
import json

def setup_logging(default_path='logging.json',default_level=logging.INFO,env_key='LOG_CFG'):
    """
        Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

#
# IMPORTANT: Put any additional includes below this line.  If placed above this
# line, it's possible required libraries won't be in your searchable path
#


#
#  main():
#

setup_logging()

if __name__ == '__main__':



    logger = logging.getLogger(__name__)

    logger.info("iniciando...")

    port = app.config['PORT']
    ip = app.config['IP']
    app_name = app.config['APP_NAME']
    host_name = app.config['HOST_NAME']

    from flask import Flask

    server = Flask(__name__, static_url_path='')
    server.wsgi_app = app
    server.run(host=ip, port=port)