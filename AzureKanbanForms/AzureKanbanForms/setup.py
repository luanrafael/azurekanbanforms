#!/usr/bin/python
# -*- coding: utf-8 -*-
from setuptools import setup


setup(name='api-superclient',
      version='1.0',
      description='Aplicação de propriedade da empresa SuperClient Solutions LTDA',
      author='Luan Rafael',
      author_email='luan@epsoft.com.br',
      url='http://superclient.com.br',
     install_requires=['Flask>=0.10','requests==2.7.0','python-dateutil==2.5.3','SQLAlchemy==1.0.8','requests==2.7.0','configparser==3.5.0','pymysql'],
     )
