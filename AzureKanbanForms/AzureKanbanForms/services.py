#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'

import json
from models import service


def load(id):

    s = service.get(id)

    return json.loads(s.data) if s is not None else {}