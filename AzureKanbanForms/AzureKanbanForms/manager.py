#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Luan Rafael'

import datetime

from AzureKanbanForms.Utils import utils
import kanban
import services
import sys
import json
import logging

applogger = logging.getLogger(__name__)

class Logger():
    message = ''

    def __init__(self):
        self.message = '-----------------\n' + str(datetime.datetime.now()) + '\n-----------------\n'

    def log(self, message, end='\n'):
        try:
            if type(message) != unicode or type(message) != unicode:
                message = unicode(str(message), errors='ignore')
            self.message += message + end
        except Exception as e:
            self.message += "??????"

class Manager():

    logger = None
    instance = 0
    task = 0
    forms = None

    def __init__(self):
        print("ok")

    def proc(self, service, cliente, instance, task):
        self.logger = Logger()

        cfg = utils.get_config()

        clientes_cfg = cfg["CLIENTES"][cliente]

        kanbanConsumer = kanban.KanbanConsumer(clientes_cfg['ORGANIZATION_ID'], clientes_cfg['TOKEN'], cfg,
                                               clientes_cfg['PROCESS_ID'])
        self.kanbanConsumer = kanbanConsumer

        forms = kanbanConsumer.get_forms(instance, task)

        if forms['instance'] is None and forms['task'] is None:
            self.logger.log('NAO ENCONTREI A INSTANCIA')
            return self.logger.message

        work_service = services.load(service)
        self.forms = {'processo': forms['instance'], 'tarefa': forms['task']}
        self.instance = instance
        self.task = task

        for wk in work_service:

            stop_if_true = wk['parar_se_verdadeiro'] if 'parar_se_verdadeiro' in wk else True
            self.logger.log(str(stop_if_true))
            if self.proc_service(wk, self.forms) and stop_if_true:
                return self.logger.message

        return self.logger.message


    def proc_service(self, work_service, forms):
        self.logger.log('==============================')
        self.logger.log(work_service['descricao'])

        if not self.is_valid(work_service, forms):
            self.logger.log('INVALIDO')
            return False

        self.logger.log('acoes')
        ret_actions = self.proc_actions(work_service['acoes'], forms)

        self.logger.log(ret_actions)

        if len(ret_actions['update_forms']) > 0:
            ret_updform = self.kanbanConsumer.update_form(self.instance, forms['processo'], ret_actions['update_forms'])
            self.logger.log('updating....')
            self.logger.log(ret_updform)
            self.logger.log('==============================')
            return True

        if len(ret_actions['update_columns']) > 0:
            ret_updcolumn = self.kanbanConsumer.update_column(self.instance, ret_actions['update_columns'])
            self.logger.log('updating column....')
            self.logger.log(ret_updcolumn)
            self.logger.log('==============================')
            return True

        return False


    def is_valid(self, work_service, forms):

        conditions_keys = ['processo', 'tarefa', 'expressao']

        for key in conditions_keys:

            if not key in work_service['condicoes']:
                continue

            conditions = work_service['condicoes'][key]

            if key == 'expressao':
                if not self.verify_expression(conditions, forms):
                    return False
                continue

            self.logger.log('formulario de [' + key + ']')
            if not self.verify_confitions(conditions, forms[key]):
                return False

        return True


    def verify_confitions(self, conditions, form):
        self.logger.log('verificando condicoes...')
        if form is None:
            return False

        keys = list(conditions.keys())
        self.logger.log(keys)


        for key in keys:

            if key not in form:
                self.logger.log("Nao achei o campo [" + key + "] no formulario")
                return False

            if key not in conditions:
                self.logger.log("Nao achei o campo [" + key + "] nas condicoes")
                return False

            if form[key] is None and conditions[key].rstrip() == "":
                continue

            if type(form[key]) is str:
                form_value = form[key].lower()
            else:
                form_value = form[key]

            if type(conditions[key]) is str:
                comparsion_value = conditions[key].lower()
            else:
                comparsion_value = conditions[key]

            fform_value = form_value

            if form_value is None:
                fform_value = "None"

            if fform_value != comparsion_value:
                self.logger.log(fform_value + '!=' + comparsion_value)
                return False
            self.logger.log(fform_value + '==' + comparsion_value)
        return True

    def verify_expression(self, expression, form, show = False):
        self.logger.log('verificando expressoes...')
        if len(expression) == 0:
            return False

        fields = utils.match_fields(expression)
        self.logger.log(expression);
        for field in fields:
            data = {
                'interval': 'd',
                'n': 0,
                'form_field': field['field']
            }
            val = self.convert(data, form)

            if val == "##NAO_ACHEI_NADA##":
                val = ''

            expression = expression.replace(field['field'], val)

        applogger.info("sem fields: %s", expression)
        methods = utils.match_methods(expression)

        for method in methods:
            args = utils.match_args(method['method'])[0]
            ret = utils.call_method(args['method'], args['args'])
            expression = expression.replace(method['method'], str(ret))

        if show:
            applogger.info(expression)
        applogger.info("resultado: %s", expression)

        return eval(expression)

    def proc_actions(self, actions, forms):

        actions_exec = {'update_forms': {}, 'update_columns': {}}
        i = 1

        for action in actions:
            self.logger.log('acao %d' % i)
            i += 1

            if action['acao'] == 'alterar campo':

                if 'formula' in action:
                    value = self.convert_value(action['formula'], forms)
                else:
                    value = action['valor']

                actions_exec['update_forms'][action['campo']] = value

            if action['acao'] == 'alterar coluna':

                action_fields = [
                    {'name': 'column', 'required': True},
                    {'name': 'task', 'required': True},
                    {'name': 'owner', 'required': True}
                ]

                for action_field in action_fields:

                    if action_field['name'] not in action and action_field['required'] is True:
                        self.logger.log("nao encontrei o campo: " + action_field['name'])
                        continue

                    if utils.is_shortcode(action_field['name']):
                        actions_exec['update_columns'][action_field['name']] = self.convert_value(action_field['name'], forms)
                    else:
                        actions_exec['update_columns'][action_field['name']] = action[action_field['name']]

            if action['acao'] == 'alterar responsavel':

                if utils.is_shortcode(action['owner']):
                    actions_exec['update_owner']['owner'] = self.convert_value(action['owner'], forms)
                else:
                    actions_exec['update_owner']['owner'] = action['owner']


        return actions_exec


    def convert_value(self, formula, forms):
        register = utils.formula_dict(formula)

        if len(register) == 0:
            form_value_dict = utils.match_formfield(formula)

            if formula == '[hoje]':
                return utils.convert_date()

            if len(form_value_dict) < 2:
                form_key = 'processo'
                form_field = form_value_dict[0]
            else:
                form_key = form_value_dict[0]
                form_field = form_value_dict[1]

            if form_field in forms[form_key]:
                return forms[form_key][form_field]

            return '##NAO_ACHEI_NADA##'

        return self.convert(register[0], forms)


    def convert(self,data, forms):
        form_value_dict = utils.split_form_field(data['form_field'])
        self.logger.log('convertendo: ' + data['form_field'])

        is_today = False

        # se o tamanho do retorno e menor que dois, usa o form do processo
        if len(form_value_dict) < 2:
            form_key = 'processo'
            self.logger.log(form_value_dict)
            form_field = form_value_dict[0]
        else: # senao usa o atributo do shortcode [tarefa.field] ou [processo.field]
            form_key = form_value_dict[0]
            form_field = form_value_dict[1]

        if form_field == 'hoje':
            is_today = True
            datevalue = datetime.date.today()
            newvalue = utils.dateadd(datevalue, data['interval'], data['n'])
            return newvalue
        else:
            if not form_field in forms[form_key]:
                self.logger.log("Nao encontrei o campo [" + form_field + "] no formulario: " + str(forms[form_key]))
                return '##NAO_ACHEI_NADA##'

            form_value = forms[form_key][form_field]

            #valida se o formulário possui algum valor para aplicar a formula
            if form_value is None:
                self.logger.log("campo [" + form_field + "] do formulario: " + form_key +" nao possui valor!")
                return '##NAO_ACHEI_NADA##'

            try:

                datevalue = utils.validate_date(form_value)
                newvalue = utils.dateadd(datevalue, data['interval'], data['n'])
                return newvalue

            except:
                self.logger.log('Nao consegui converter o valor: {'+ form_value +'} em data "%y-%m-%d"')
                self.logger.log("Error: " + str(sys.exc_info()[0]))
                return '##NAO_ACHEI_NADA##'


        return '##NAO_ACHEI_NADA##'




# print(proc('',172474,24970))

# print(utils.formula_dict("[tarefa.Indicação]"))
# print(utils.match_formfield("[tarefa.Indicação]"))
