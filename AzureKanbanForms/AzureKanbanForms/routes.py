#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Luan Rafael'

from flask import Flask
from flask import Flask, request
import AzureKanbanForms.manager as manager

import io
import json
import sys
import AzureKanbanForms.officer as officer
import threading
import logging
import urllib


from AzureKanbanForms.Utils import utils

from AzureKanbanForms.models import helper, service, agenda, tarefa, xaklog
from AzureKanbanForms import app
import os


logger = logging.getLogger(__name__)

ROUTE_API_V1 = "/api/v1/"

@app.route('/.well-known/<path:path>')
def certificate(path):
    return app.send_static_file(path)


@app.route(ROUTE_API_V1 + 'teste')
def versao():
    return "ATUALIZADO..."

@app.route(ROUTE_API_V1 + "service/<id>")
def show_service(id):
    s = service.get(id)

    if s is not None:
        return s.data

    return "nok"


@app.route(ROUTE_API_V1 + "run/<id>/<cliente>/<instance>/<task>")
def run_service(id, cliente, instance, task):

    try:
        man = manager.Manager()
        ret = man.proc(id, cliente, instance, task)

        log_filename =  _path_log_services() + str(id) + '_' + str(instance) + '_' + str(task) + '.log'

        f = open(log_filename, 'w', 99999)
        f.write(ret.encode('utf8'))
        f.close()
        return "ok"
    except Exception as e:
        logging.error(e)
        return "nok"


def _path_log_services():
    return os.path.dirname(os.path.realpath(__file__)) + '\\log_services\\'


@app.route(ROUTE_API_V1 + "log/<id>/<instance>/<task>")
def log_service(id, instance, task):
    log_filename = _path_log_services() + str(id) + '_' + str(instance) + '_' + task + '.log'

    with open(log_filename, 'rU', 99999) as data_file:
        data = data_file.read()

    return data.replace('\n', '<br/>')


@app.route(ROUTE_API_V1 + "config/<id>", methods=['POST', 'GET'])
def upd_json_config(id):

    form_template = '''
            <form method='POST'>
                <textarea name='jsonconfig' style="width: 90%;margin:0 auto" rows=10></textarea>
                <br>
                <button type='submit' style="background: #4caf50;width: 90%;height: 50px;margin: 0 auto;color: #fff;font-size: 20px;">Salvar</button>
            </form>
            <br>
            <h2 style="color:red"> {{ error_message }} </h2>
        '''

    if request.method == 'GET':
        return form_template.replace('{{ error_message }}', '')

    if request.method == 'POST':

        try:
            json.loads(request.form['jsonconfig'])
        except ValueError:
            return form_template.replace('{{ error_message }}', request.form['jsonconfig'] + '<br>Json Inválido')

        dbsession = helper.DBSession('kanbanforms')

        s = service.Service(id,request.form['jsonconfig'])

        dbsession.save(s)

        output = dbsession.get(service.Service,id)

        if output is not None:
            return output.data

        dbsession.dispose()

        return output


@app.route(ROUTE_API_V1 + "agenda/<clinica>/<medico>", methods=['POST'])
def sinc_agenda(clinica, medico):
    logger.info("sinc_agenda %s - %s", clinica, medico)

    ultima_agenda = agenda.get_last(clinica, medico)

    agenda_atual = agenda.Agenda(medico, request.json, clinica)

    try:
        dbsession = helper.DBSession()
        dbsession.save(agenda_atual)
        output = {"error": False, "message": agenda_atual.id}
    except:
        output = {"error": True, "message": str(sys.exc_info())}

    if ultima_agenda is None:
        officer_thread = threading.Thread(target=officer.sincronizar_agenda,
                                          args=(clinica, medico, agenda_atual.consultas, "[]"))
    else:
        officer_thread = threading.Thread(target=officer.sincronizar_agenda,
                                          args=(clinica, medico, agenda_atual.consultas, ultima_agenda.consultas))

    officer_thread.daemon = True
    officer_thread.start()

    return json.dumps(output)




@app.route(ROUTE_API_V1 + "broker/<fingerprint>/<id_acao>/<acao>")
def broker(fingerprint, id_acao, acao):

    acao = urllib.unquote(urllib.unquote(acao))

    nova_tarefa = tarefa.Tarefa(id_acao, acao, fingerprint)

    try:

        if 'dev' in fingerprint:
            dbsession = helper.DBSession('dev')
        else:
            dbsession = helper.DBSession('kanbanforms')
        
        dbsession.save(nova_tarefa)
        acao = acao.replace("'", "`")
        cmd = "procTarefaAssyc '%s^%s'" %(str(nova_tarefa.id),acao)

        dbsession.dispose()

        request_thread = threading.Thread(target=utils.send, args=(fingerprint,cmd))
        request_thread.daemon = True;
        request_thread .start()

    except Exception as e:
        return "nao consegui salvar a tarefa\n" + str(e)

    return "tarefa recebida com sucesso";


@app.route(ROUTE_API_V1 + "xakomigo/log/<tipo>/<dt_log>", methods=['POST'])
def grava_log(tipo, dt_log):


    dbsession = helper.DBSession('xakomigo_logs')
    session = dbsession.get_session()
    requisicao = request.json
    novoLog = xaklog.Xaklog(requisicao['token'], tipo, dt_log, requisicao['conteudo'])

    xaklogSalvo = xaklog.get(requisicao['token'], tipo, dt_log, session, False)

    if xaklogSalvo:
        novoLog.id = xaklogSalvo.id
        xaklogSalvo.conteudo = novoLog.conteudo
        session.commit()
        session.expunge(xaklogSalvo)
    else:
        dbsession.save(novoLog, True)

    dbsession.dispose()

    return requisicao['conteudo']

@app.route(ROUTE_API_V1 + "xakomigo/log/<token>/<tipo>/<dt_log>", methods=['GET'])
def get_log(token, tipo, dt_log):
    return xaklog.get(token, tipo, dt_log).conteudo

@app.route("/test")
def test():
    return json.dumps(utils.get_config())

@app.route("/ok")
def ok():
    return "okokokok"

@app.route("/")
def home():
    return "ok..."

